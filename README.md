how the framework was designed
Install DK on the machine
Map the Jdk in the system environment variables by adding the Jdk installation pat and the bin folder
Created a maven project 
Added karate framework dependencies to the pom file
Used the cucumber BDD framework, installed cucumber plugin

To Run the test
The feature files can be run independently by right clicking and selecting run feature file
Also the scenarios can be run independently by right clicking a scenario and selecting run scenario

Reporting
A report will be generated after running the tests under: target\surefire-reports\src.test.resources.test.html
The project was uploaded to gitlab under t link: https://gitlab.com/modibam/globalkineticassessment
