Feature: fetching User Details
  Scenario: testing the get call for User Details
    Given url 'https://reqres.in/api/users/2'
    When method GET
    Then status 200

  Scenario: testing the get call for User Details response body
    Given url 'https://reqres.in/api/users/2'
    When method GET
    Then status 200
    And match response.data.id contains 2
    And match response.data.email contains 'janet.weaver@reqres.in'
    And match response.data.first_name contains 'Janet'
    And match response.data.last_name contains 'Weaver'
    And match response.data.avatar contains 'https://reqres.in/img/faces/2-image.jpg'

